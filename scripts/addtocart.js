module.exports = async function(context, commands) {
  await commands.navigate(
    'https://rxbarm2-stage.smarterspecies.com/sample-pack.html'
  );
 
  try {
    await commands.measure.start('Stage_AddToCart');

    await commands.click.byIdAndWait('product-addtocart-button');

    return commands.measure.stop();

  } catch (e) {
    throw e;
  }
};