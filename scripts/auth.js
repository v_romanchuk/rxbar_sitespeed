module.exports = async function(context, commands) {
  await commands.navigate(
    'https://rxbarm2-stage.smarterspecies.com/customer/account/login/'
  );
 
  try {
    await commands.addText.byId('vromanchuk@gorillagroup.com', 'email');
    await commands.addText.byId('123Vr$321', 'pass');
    
    await commands.measure.start('Stage_Authorization');

    await commands.click.byIdAndWait('send2');

    return commands.measure.stop();

  } catch (e) {
    throw e;
  }
};