module.exports = async function(context, commands) {
  await commands.navigate('https://rxbarm2-stage.smarterspecies.com/checkout/cart');
 
  try {
    await commands.measure.start('Stage_Checkout');

    await commands.navigate('https://rxbarm2-stage.smarterspecies.com/checkout/#shipping');
    await commands.js.run('window.scrollTo(0,document.body.scrollHeight);');
    await commands.wait.byId('s_method_shqups_GND',30000);
    await commands.click.byId('s_method_shqups_GND');
    await commands.click.bySelectorAndWait('#shipping-method-buttons-container button');
    await commands.wait.byId('subscribe_pro_vault_119805',30000);
    await commands.click.byId('subscribe_pro_vault_119805');
    await commands.js.run('window.scrollTo(0,500);');
    await commands.click.bySelectorAndWait('.payment-methods .payment-method._active button.checkout');
    await commands.wait.byId('locator',30000);

    return commands.measure.stop();

  } catch (e) {
    throw e;
  }
};